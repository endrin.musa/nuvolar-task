#!/bin/bash

################## CHECK IF CONTAINERS ARE RUNNING #########################################################

#### API GATEWAY ####
check_api=`docker ps | grep api`
if [ -z "$check_api" ]
then
      echo "API not running, running container"
      docker run -p 8080:8080 -d --name api-gateway -t nuvolar/api-gateway -e ORDER_SERVICE_URL='http://localhost:8081'
else
      echo "API is running"
fi


#### ORDER SERVICE ####
check_order=`docker ps | grep order`
if [ -z "$check_order" ]
then
      echo "ORDER SERVICE not running"
      docker run -p 8081:8080 -d --name order-service -e CUSTOMER_SERVICE_URL='http://localhost:8082' -t nuvolar/order-service
else
      echo "ORDER SERVICE is running"
fi



#### CUSTOMER SERVICE ####
check_customer=`docker ps | grep customer`
if [ -z "$check_customer" ]
then
      echo "CUSTOMER SERVICE  not running"
      docker run -p 8082:8080 -d --name customer-service -t nuvolar/customer-service
else
      echo "CUSTOMER SERVICE  is running"
fi
