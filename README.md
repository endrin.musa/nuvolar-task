
# Nuvolar Task - Endrin Musa

Below, you can find all the information regarding this project in details, including:
- Project Description
- System Specification & Tools Used
- Infrastructure Diagram
- Deployment Instructions


### Project Description

For this project, I have chosen a bare metal server hosted on Hetzner Cloud running **Ubuntu 20.04**. It used the Docker Container runtime to run the Docker Images.
The Docker images used were the ones provided on the task definition. 
GitLab was used as a code repository and CI/CD solution. The pipeline is triggered on commit which afterwards invokes an Ansible playbook that first checks for prerequisites and if not, installs all the necessary software to run the application.
As a domain I have used my own website domain **(www.endrinmusa.dev)** and the **API** microservice is accessible using a subdomain (**api.endrinmusa.dev**).
On the background, NGINX is used as a reverse proxy to forward all requests coming in port 443 for **api.endrinmusa.dev** to the respective Docker Container running on a specific port.
The communication with the API is encrypted with SSL issued by Letsencrypt.
All the other ports are blocked by default using the **Uncomplicated Firewall**.

### System Specification & Tools Used

This section describes system specification and also lists the tools used to implement this solution, along with a short description of the purpose of that tools in this project.

**Operating System** : Ubuntu 20.04 LTS  
**System Specification**: 2 vCPUs, 2GB RAM, 250GB SSD, 1GB Uplink  
**Container Runtime**: Docker   
**Code Repository**: GitLab  
**CI/CD Pipeline**: GitLab CI/CD  
**Infrastructure as Code**: Ansible  
**Reverse Proxy**: NGINX 
**SSL Issuer**: Let's Encrypt 


### Infrastructure Diagram

![alt text](https://i.ibb.co/bXMV0hZ/nuvolar-project.jpg)


## Deployment

Below, you can find instructions about deploying this application.  

After you clone the repository, everything has been simplified to a single command which will spin up the application on localhost for you.

**Please note that the only pre-requisite of running this application is a clean installation of Ubuntu 20.04**

- Clone the repository
```bash
  git clone https://gitlab.com/endrinmusa/nuvolar-project
```

- Run the application (localhost, for the Nuvolar team to reproduce)
```bash
  ansible-playbook nuvolar-localhost.yaml
```

- Run the application (production environment, using CI/CD)
```bash
  ansible-playbook nuvolar-project.yaml
```



